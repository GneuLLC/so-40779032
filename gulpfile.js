(() => {
    "use strict";

    var gulp = require("gulp"),
        jasmineBrowser = require("gulp-jasmine-browser");

    gulp.task("jasmine", () => {
    	return gulp.src("test/*.js")
    		.pipe(jasmineBrowser.specRunner({console: true}))
    		.pipe(jasmineBrowser.headless());
    });
})();